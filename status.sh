#!/bin/sh
###
#Author: Arthur Lee <leea3@hawaii.edu>
#Date: 2 Feb 2022
#
#Simple shell script to display user and server info
###


echo I am
whoami
echo The current working directory is
pwd
echo The system I am on is
uname -n
echo The Linux version is
uname -r
echo The Linux distribution is
cat /etc/system-release
echo The system has been up for 
uptime
echo The amount of disk space I\'m using in KB is
du -ks ~

