#!/bin/sh
###
#Author: Arthur Lee <leea3@hawaii.edu>
#Date: 2 Feb 2022
#
#Simple shell script to show how to assign strings to variables and display it onto the screen
###

NAME="Arthur"
FUNFACT="my left finger is double-jointed"


echo I am $NAME. A fun fact about me is: $FUNFACT.
