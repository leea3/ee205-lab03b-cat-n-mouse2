#!/bin/bash
###
#Author: Arthur Lee <leea3@hawaii.edu>
#Date: 2 Feb 2022
#
#Cat n Mouse game written in shell script
###

DEFAULT_MAX_NUMBER=2048
DEFAULT_MIN_NUMBER=1

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

if [ "$#" -ne 1 ]
then
   GAME_MAX_NUMBER=$DEFAULT_MAX_NUMBER
else
   GAME_MAX_NUMBER=$1
   if [ $GAME_MAX_NUMBER -lt 1 ]
   then
      echo Entered max value is not an integer \>\= 1
      exit
   fi
fi

# echo The max value is $THE_MAX_VALUE

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

# echo The number I\'m thinking of is $THE_NUMBER_IM_THINKING_OF

echo Ok cat, make a guess...

read A_GUESS


while [ $A_GUESS -ne $THE_NUMBER_IM_THINKING_OF ]
do

   if [ $A_GUESS -gt $GAME_MAX_NUMBER ]
   then
      echo You must enter a value that\'s \<\= $GAME_MAX_NUMBER
      read A_GUESS
   fi

   if [ $A_GUESS -lt $DEFAULT_MIN_NUMBER ]
   then
      echo You must enter a value that\'s \>\= $DEFAULT_MIN_NUMBER
      read A_GUESS
   fi



   if [ $A_GUESS -gt $THE_NUMBER_IM_THINKING_OF -a $A_GUESS -le $GAME_MAX_NUMBER -a $A_GUESS -ge $DEFAULT_MIN_NUMBER ]
   then
      echo No Cat... the number I\'m thinking of is less than $A_GUESS
      read A_GUESS
   fi

   if [ $A_GUESS -lt $THE_NUMBER_IM_THINKING_OF -a $A_GUESS -le $GAME_MAX_NUMBER -a $A_GUESS -ge $DEFAULT_MIN_NUMBER ]
   then
      echo No Cat... the number I\'m thinking of is greater than $A_GUESS
      read A_GUESS
   fi
done

echo You got me...
cat << "EOF" 
 |\__/,|   (`\
 |_ _  |.--.) )
 ( T   )     /
(((^_(((/(((_/
EOF
